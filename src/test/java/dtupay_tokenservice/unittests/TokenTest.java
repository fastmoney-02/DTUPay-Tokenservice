package dtupay_tokenservice.unittests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dtupay_tokenservice.models.Token;
import models.UserId;

public class TokenTest {

	@Test
	public void equalsTest() {
		Token token1 = new Token(new UserId("1"));
		Token token2 = token1;
		
		assertTrue(token1.equals(token2));
	}
	
	@Test
	public void notEqualsTest() {
		Token token1 = new Token(new UserId("1"));
		Token token2 = new Token(new UserId("1"));
		
		assertFalse(token1.equals(token2));
	}
}
