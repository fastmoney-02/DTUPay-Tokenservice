package dtupay_tokenservice.unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import dtupay_tokenservice.TokenDatabase;
import dtupay_tokenservice.TokenManager;
import dtupay_tokenservice.interfaces.ITokenRepository;
import dtupay_tokenservice.models.Token;
import models.UserId;
import models.TokenId;

public class TokenManagerTest {

	TokenManager tokenManager;
	
	@Before 
	public void setUp(){
		ITokenRepository tokenRepository = TokenDatabase.getInstance();
		tokenManager = new TokenManager(tokenRepository);
	}
	
	@Test
	public void requestToken(){
		UserId userId = new UserId("TestReuestToken");
		assertEquals(2, tokenManager.requestToken(userId, 2).size());
	}
	
	@Test
	public void requestZeroToken(){
		assertEquals(0, tokenManager.requestToken(new UserId("TestRequestZeroTokens"), 0).size());
	}
	
	@Test
	public void requestToManyToken(){
		assertEquals(0, tokenManager.requestToken(new UserId("TestRequestManyTokens"), 7).size());
	}
	
	@Test
	public void requestTokenWhenHaveTooMany(){
		UserId userId = new UserId("TestWhenTooMany");
		tokenManager.requestToken(userId,4);
		assertEquals(0, tokenManager.requestToken(userId, 4).size());
	}
	
	@Test
	public void consumeToken(){
		UserId userId = new UserId("TestConsume");
		ArrayList<Token> tokens = tokenManager.requestToken(userId,4);
		assertTrue(tokenManager.consume(tokens.get(0).getId()));
	}
	
	@Test
	public void consumeFakeToken(){
		Token token = new Token(new UserId("Test"));
		assertFalse(tokenManager.consume(token.getId()));
	}
	
	@Test
	public void consumeTokenTwice(){
		UserId userId = new UserId("TestConsumeTwice");

		ArrayList<Token> tokens = tokenManager.requestToken(userId,4);
		
		Token t = tokens.get(0);
		
		tokenManager.consume(t.getId());
		assertFalse(tokenManager.consume(t.getId()));
	}
	
	@Test
	public void getSingleTokenTest() {
		UserId userId = new UserId("TestGetSingleToken");
		ArrayList<Token> tokens = tokenManager.requestToken(userId,1);
		
		Token t = tokens.get(0);
		
		TokenId tokenId = t.getId();
		
		t = tokenManager.getSingleToken(tokenId);
		tokenManager.consume(t.getId());
	}
	
}
