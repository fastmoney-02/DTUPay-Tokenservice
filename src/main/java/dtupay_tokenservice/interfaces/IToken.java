package dtupay_tokenservice.interfaces;

import java.util.ArrayList;

import dtupay_tokenservice.models.Token;
import models.TokenId;
import models.UserId;

/**
 * The Interface IToken.
 */
public interface IToken {
	
	/**
	 * Consume.
	 *
	 * @param token tto be consumed
	 * @return true, if successful
	 */
	public boolean consume(TokenId token);
	
	/**
	 * Request token.
	 *
	 * @param userId of the user who requested tokens
	 * @param amount of tokens
	 * @return the array list of token ids
	 */
	public ArrayList<Token> requestToken(UserId userId, int amount);
	
	/**
	 * Gets the single token.
	 *
	 * @param tokenId id of the token wanted
	 * @return the token found
	 */
	public Token getSingleToken(TokenId tokenId);
	
	
}
