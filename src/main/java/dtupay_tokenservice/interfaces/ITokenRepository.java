package dtupay_tokenservice.interfaces;

import java.util.ArrayList;

import dtupay_tokenservice.models.Token;
import models.TokenId;
import models.UserId;

/**
 * The Interface ITokenRepository.
 */
public interface ITokenRepository {
	
	/**
	 * Gets the tokens.
	 *
	 * @param id of the user
	 * @return the users tokens
	 */
	ArrayList<Token> getTokens(UserId id);
	
	/**
	 * Gets the token.
	 *
	 * @param id of the token
	 * @return the token found
	 */
	Token getToken(TokenId id);
	
	/**
	 * Adds the token.
	 *
	 * @param token that should be added
	 * @return true, if successful
	 */
	boolean addToken(Token token);
	
	/**
	 * Removes the token.
	 *
	 * @param token id of the token that should be removed
	 * @return true, if successful
	 */
	boolean removeToken(TokenId token);
}
