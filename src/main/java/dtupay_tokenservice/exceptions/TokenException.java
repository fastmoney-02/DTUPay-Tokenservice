package dtupay_tokenservice.exceptions;

/**
 * The Class TokenException.
 */
@SuppressWarnings("serial")
public class TokenException extends Exception{
	
	/**
	 * Instantiates a new token exception.
	 *
	 * @param message the message
	 */
	public TokenException(String message)
	{
		super(message);
	}

}
