package dtupay_tokenservice;

import java.util.ArrayList;

import dtupay_tokenservice.interfaces.IToken;
import dtupay_tokenservice.interfaces.ITokenRepository;
import dtupay_tokenservice.models.Token;
import models.TokenId;
import models.UserId;

/**
 * The Class TokenManager.
 */
public class TokenManager implements IToken {	

	/** The database. */
	private ITokenRepository database;

	/**
	 * Instantiates a new token manager.
	 *
	 * @param database the database
	 */
	public TokenManager(ITokenRepository database) {
		this.database = database;
	}

	/**
	 * Consume.
	 *
	 * @param tokenId if of the token
	 * @return true, if successful
	 */
	public boolean consume(TokenId tokenId)
	{
		Token token = database.getToken(tokenId);
		if(isValid(token)) {
			if(database.removeToken(tokenId)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Request token.
	 *
	 * @param userId the user who wants more tokens
	 * @param amount of tokens
	 * @return the array of tokens
	 */
	public ArrayList<Token> requestToken(UserId userId, int amount) 
	{
		ArrayList<Token> tokens = new ArrayList<Token>();
		if(canGetMoreTokens(userId, amount)) {
			for (int i = 0; i < amount; i++) {
				Token token = new Token(userId);
				if (database.addToken(token)) {
					tokens.add(token);
				}
			}
			return tokens;
		}
		return tokens;

	}

	/**
	 * Gets the single token.
	 *
	 * @param tokenId id of the token
	 * @return the found token
	 */
	public Token getSingleToken(TokenId tokenId) {
		Token token = database.getToken(tokenId);
		return token;
	}

	/**
	 * Can get more tokens.
	 *
	 * @param userId who wants more token
	 * @param amount of tokens wanted
	 * @return true, if user can get more tokens
	 */
	private boolean canGetMoreTokens(UserId userId, int amount) {
		return database.getTokens(userId).size() <= 1 && amount <= 5 && amount >=1;
	}

	/**
	 * Checks if is valid.
	 *
	 * @param token to be checked
	 * @return true, if is valid
	 */
	private boolean isValid(Token token)
	{	
		if(token != null) {
			ArrayList<Token> tokens = database.getTokens(token.getUserId());
			for (Token t : tokens) {
				if(t.getId().equals(token.getId())) {
					return true;
				}
			}
		}
		return false;
	}

}
