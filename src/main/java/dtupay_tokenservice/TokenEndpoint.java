package dtupay_tokenservice;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//import DTUPay.Database;
import dataTransferObject.RequestTokensArgs;
import dataTransferObject.TokenDTO;
import dtupay_tokenservice.exceptions.TokenException;
import dtupay_tokenservice.interfaces.ITokenRepository;
import dtupay_tokenservice.models.Token;
import models.UserId;
import models.TokenId;

/**
 * The Class TokenEndpoint.
 */
@Path("/token")
public class TokenEndpoint {
	
	/** The database. */
	ITokenRepository database = TokenDatabase.getInstance();
	
	/** The token manager. */
	TokenManager tokenManager = new TokenManager(database);
	
	/**
	 * Request tokens.
	 *
	 * @param args the args
	 * @return the array list
	 * @throws TokenException the token exception
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<String> requestTokens(RequestTokensArgs args) throws TokenException{
		ArrayList<Token> result = tokenManager.requestToken(new UserId(args.getUserId()), args.getTokenAmount());
		if (result.size() == 0) {
			throw new TokenException("No token returned");
		}
		ArrayList<String> cleanedTokens = new ArrayList<String>();
		for(Token t : result) {
			cleanedTokens.add(t.getId().getValue());
		}
		return cleanedTokens;
	}
	
	/**
	 * Gets the single token.
	 *
	 * @param tokenId the token id
	 * @return the single token
	 */
	@GET
	@Path("{tokenId}")
	@Produces(MediaType.APPLICATION_JSON)
	public TokenDTO getSingleToken(@PathParam("tokenId") String tokenId) {
		Token token = tokenManager.getSingleToken(new TokenId(tokenId));
		return new TokenDTO(token.getId().getValue(), token.getUserId().getValue());
	}
	
	/**
	 * Consume token.
	 *
	 * @param token the token
	 * @return true, if successful
	 */
	@PUT
	@Path("{tokenId}/consume")
	@Consumes(MediaType.TEXT_PLAIN)
	public boolean consumeToken(@PathParam("tokenId") TokenId token) {
		return tokenManager.consume(token);
	}

}
