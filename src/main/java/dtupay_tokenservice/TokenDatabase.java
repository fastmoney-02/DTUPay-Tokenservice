package dtupay_tokenservice;

import java.util.ArrayList;
import java.util.stream.Collectors;

import dtupay_tokenservice.interfaces.ITokenRepository;
import dtupay_tokenservice.models.Token;
import models.TokenId;
import models.UserId;

/**
 * The Class TokenDatabase.
 */
public class TokenDatabase implements ITokenRepository{
	
	/** The tokens. */
	private ArrayList<Token> tokens;
	
	/** The database. */
	private static TokenDatabase database;
	
	/**
	 * Instantiates a new token database.
	 */
	private TokenDatabase() {
		tokens = new ArrayList<Token>();
	}
	
	/**
	 * Gets the single instance of TokenDatabase.
	 *
	 * @return single instance of TokenDatabase
	 */
	public static TokenDatabase getInstance() {
		if (database == null) {
			database = new TokenDatabase();
		}
		return database;
	}

	/**
	 * Gets the tokens.
	 *
	 * @param id the id
	 * @return the tokens
	 */
	@Override
	public ArrayList<Token> getTokens(UserId id) {
		return (ArrayList<Token>) tokens.stream().filter(t -> t.getUserId().equals(id)).collect(Collectors.toList());
	}

	/**
	 * Gets the token.
	 *
	 * @param id the id
	 * @return the token
	 */
	@Override
	public Token getToken(TokenId id) {
		return tokens.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);
	}

	/**
	 * Adds the token.
	 *
	 * @param token the token
	 * @return true, if successful
	 */
	@Override
	public boolean addToken(Token token) {
		tokens.add(token);
		return true;
	}

	/**
	 * Removes the token.
	 *
	 * @param tokenId the token id
	 * @return true, if successful
	 */
	@Override
	public boolean removeToken(TokenId tokenId) {
		return tokens.removeIf(t -> t.getId().equals(tokenId));
	}

}
