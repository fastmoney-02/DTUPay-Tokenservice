package dtupay_tokenservice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * The Class RestApplication.
 */
@ApplicationPath("/")
public class RestApplication extends Application {
}