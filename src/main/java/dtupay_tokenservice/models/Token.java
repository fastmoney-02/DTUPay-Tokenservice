package dtupay_tokenservice.models;

import java.util.UUID;

import models.TokenId;
import models.UserId;

// TODO: Auto-generated Javadoc
/**
 * The Class Token.
 */
public class Token {
	
	/** The id. */
	private TokenId id;
	
	/** The user id. */
	private UserId userId;
	
	/**
	 * Instantiates a new token.
	 */
	public Token() {}
	
	/**
	 * Instantiates a new token.
	 *
	 * @param userId the user id
	 */
	public Token(UserId userId)
	{
		this.id = generateId();
		this.userId = userId;
	}

	/**
	 * Generate id.
	 *
	 * @return the token id
	 */
	private TokenId generateId() {
		return new TokenId(UUID.randomUUID().toString());
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public TokenId getId()
	{
		return id;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UserId getUserId()
	{
		return userId;
	}
	
	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		
		if(obj instanceof Token)
		{
			return ((Token)obj).id == id;
		}
		return false;
	}
}
